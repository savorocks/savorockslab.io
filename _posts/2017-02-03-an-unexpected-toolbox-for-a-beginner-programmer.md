---
title:  "An Unexpected Toolbox for a Beginner Programmer"
date:   2017-02-03 18:30:00 +0100
author: Savo Djuric
categories: [Programming, Music]
tags: [python, nixos, tutorial, github, sands, colemak]

---

In this post I want to tell you a little about the setup that I use to write code
and similar stuff (by that I mean HTML and CSS which I don't really think of as code).
This is not going to be a precise tutorial, but more like a story how I started using
things that I'll describe further down this post

---

## Why I decided to write about this, particularly Emacs?

Well, I know there are a lot of different IDEs and editors out there and there is a
constant debate over which one is better, but it seems that a lot of people hold view
that Emacs is hard to learn, especially for a beginner. I must disagree with that and
in the following text I'll try to explain why.

## Who am I and how I got interested in programming?

I've been playing guitar for almost 10 years now, and during
that time I formed various kinds of bands, but mostly of rock and hard rock genres.
We often wrote songs, but being broke as all young musicians are, had no money to spend
on recording sessions. Since I was really into computers from the first time I had acces
to them (when I was about 11 years old) and always interested into tampering around and
tweaking them, I decided to try to record our songs. I didn't even have a proper audio
interface, all I had was an old Creative Sound Blaster PCI card. But, with our relatively
cheap mixer board and various samples I managed to pull it off.

You may now ask yourself "OK, but what does all this have with programming?"
Well, it kind of does. That was the point when I started to get more and more into audio
production,and learned a lot about it over the years. My brother, who at that time knew
I could use some work (I was fresh out of high school and didn't enroll into college)
connected me with a friend of his who had a small business that did audio and video support
for various events. They didn't exactly need new employees, but they thought that I can
develop small apps in Adobe Flash (which uses ActionScript language) for the events
they were hosting. I started to learn the language, and to develop some apps, but the
proccess turned out to be a lot slower than they expected and the arrangement fell apart.

One thing that frustrated me the most was that I was making lots of typos when I was
learning/writing those apps. You probably know that ActionScript language is very similar
to JavaScript, which has a lot of curly brackets and semicolons in it. Those characters
are in the top and bottom corners of the keyboard, and require a shift button to be pressed
with the key associated with them. The result was that I constantly kept my eyes switching
between the screen and the keyboard, and made whole process of learning and writing slow and
painful.

## Enter Colemak

When I told my brother about that he was not shocked. The conversation, according to my memory now,
3 years later, went roughly like this:

-"What keyboard layout are you using"

-"Keyboard layout? What do you mean?"

-"Exactly as I suspected" said he. "You use the default layout, which is QWERTY, and it is not very finger-friendly."

I was not sure what he was trying to say. Then he continued:

-"There is a better layout out there, called **[Colemak](https://colemak.com/ "Colemak Website")**.
Its design allows you to type much faster, and the hand position is more natural."

I was very interested in what I was hearing.

-"Oh, and another thing", said he. "Since you want to switch to a new layout, it is required to learn touch typing.
There is no chance of finding the colemak layout keyboard in the stores. And besides, you want to type as fast
as you can."

So he showed me a program called **[Klavaro](http://klavaro.sourceforge.net/en/ "Klavaro Website")**,
which is described as 'touch typing tutor'.

I decided to switch to *colemak* immediately and to do *Klavaro* excercises every day.
I still remember those first days trying to respond to my friends facebook chats,
typing several times slower than I would do with the standard qwerty layout. But after
a week or so using *Klavaro* to learn *colemak* I was typing as fast as I was on a qwerty
layout, the only difference now is that I didn't have to look at the keyboard even once.
**What a relief was that!** I felt so powerful. In no time I was typing notably faster and
even some friends begun noticing how fast my responses were online and how much more words
I managed to type while all they could write was "Hello, how's your day?". From that point
I couldn't recommend *colemak* enough. It is a killer layout useful for everyone who wants
to type faster and easier, from programmers to secretaries, writers etc.

## Emacs, what's SandS and why you need it!

*My Emacs setup:*
![My Emacs Setup](/my-emacs-setup-screenshot.png)

So for about two years I was using *colemak*, but didn't really write any code. I had
moderate success with my band(s) on the local scene and I really enjoyed playing with
many musicians there. On top of that I continued to record all the material that I
wrote with my various bands and with every step I got better and beter, so I didn't
really think about writing code. Then my brother stepped in again and suggested that
I try to make a band website, since I already had some previous experience with coding
(see above).

-"All you need to learn is HTML and CSS. You already had learned a little
of ActionScript, this should be a lot easier. And you've become good at touch typing,
with *colemak* layout no less. This should be easy for you."

At the time we were talking
about that I noticed the *text editor/IDE* that he was using. Oh yeah, my brother is a
seasoned programmer. I didn't mention this before, have I?

Anyway, the text editor that
he was using was **[Emacs](https://www.gnu.org/software/emacs/ "GNU Emacs")**
with a theme called *cyberpunk*. From the moment I saw it I
fell in love with it. It was mysterious as much it was beautiful. The code in it looked
amazing. I wanted to write my code in it. I told him that right then and there.

He was all like "Are you sure? Much better coders than you have tried to learn to used it for
some time, then gave up because it was different than any other editors that they were
used to!".

-"Yeah." I said. "But you didn't count on one thing"

-"And what is that?" he asked, his face showing that he was puzzled.

-"I didn't use any other editors. So
I don't have anything to compare it to. The only commands that I used with text was
cut/copy/paste, and that's about it."

-"OK, as you wish, but may I suggest something?"

-"Yes, of course. Shoot."

-"Since *Emacs* is an editor that uses a lot of modifier keys (Control,
Alt and Shift) the best thing to add to your *colemak* layout is a modified position of
Control, Alt and Shift keys.

I was all ears.

He continued: "The best thing to do, if
you want to keep your hands in natural position while typing is to map the ctrl key
right of the spacebar and your alt key left of the spacebar (on most layouts alt is
already left of the spacebar).

I noticed he didn't mention the shift key. I asked him about it.

-"Ah, you see this is really the most useful one. You can't let your fingers
constantly stretch out to reach the shift key everytime you need it. So someone was smart
enough to map the shift key to the spacebar."

I was really confused. "But what about the spacebar?"

-"What about it?"

-"Well if you map shift to the spacebar, where will the spacebar go?"

-"Ah, that's the beauty of it. It stays right where it was!"

-"Huh?" Now he really lost me. But not for long.

He continued: "That shift remapping to spacebar
is called **SandS** (pronounced as "s and s", meaning "Shift AND Space"). That's because both
the shift key and space are mapped to the spacebar."

-"Yeah, but how? Show me!"

Then he
proceeded -"When you press spacebar and hold it, it acts like a shift key. While holding it
you press another key and when you release them it gives you the key it would give you
as if you were holding the physical shift key and normal key" (for example, if you want to write
capital letter a you will press the spacebar and a key and it will spit out the A).

-"Awesome. And the regular space key?"

-"It's very similar to what it was before. You press it, and
if you don't press any button in combination with it, it returns you the space. And all this
is useful because now you use your thumbs to type ctrl, alt, shift or space, so you don't
have to move your fingers from the home row for that."

*Colemak + Sands + Ctrl/Alt Remapping*:
![My keyboard layout](/colemak.png)

When I'd learned *Colemak* I thought that it couldn't get any better. But with *SandS* and
remapped Ctrl and Alt that was whole other level. I've been using that mapping for about
a year now, and cannot imagine going back to the previous one. It really grew on me.

**One important thing!** I now
remembered that I forgot to mention the remapping of the backspace key to Caps Lock key.
In linux, this is done automatically, since you don't have to install *colemak*, it's already
listed when you go to settings to change the layout. In Windows, on the other hand, you
need to download and install *colemak*, and you need to apply the registry patch which will
then remap your backspace to caps lock key. This remapping is also very useful because
when you make a typo you don't have to reach with your pinky finger for the backspace, it
is simply one key left of the a key, which is far easier to reach.

*Screenshot of Klavaro running on windows, qwerty layout displayed:*
![Klavaro Screenshot, QWERTY](/Klavaro1.png)

**And another important thing!**
Don't get me wrong, I had my struggles with this. It wasn't as smooth as it may apper
from this article. My brother exposed me to *colemak* several times before I actually
decided to try it. Why was I resistful? I don't know exactly, but I suspect when you
are used to certain ways it's hard to decide to change. At first I thought that I
don't need to change my layout, that I simply wasn't talented or that I am not that
good at typing simply because I didn't do much typing. The hardest part about this
was actually to decide to change my ways. When I did (and I am so glad that I did)
it was relatively easy to learn to type fast without looking at the keys. And as for
*SandS*: I was using Windows at the time I switched to this, and it was really hard
to configure it, I spent months searching online, then I found [this script](https://gist.github.com/acple/5411138 "Sands Script")
for [AutoHotkey](http://ahkscript.org/ "AutoHotkey Website") and it worked almost
as good as *SandS* on Linux. The only thing that bothers me, but apparently not so much
to actually learn in depth how *AutoHotkey* scripting works to change it. With this script,
when I press and hold space it acts as a shift key BUT after 2-3 seconds, if I
don't press another key to combine it with, it switches back to being regular space.
As you can see I posted a comment on the script's github page asking how to fix it,
but still haven't recieved an answer. If you know how to change this I'll be immensely
grateful to you if you share it!

So, if you have doubts about switching to this layout because it feels it's hard stop
having them. Stop having them right now! Believe me, you will struggle for a short
period of time (I'm talking about couple of weeks in the worst case scenario) and
the benefits are huge. If you take my word for it and try this out, I'm absolutely
sure that you will not regret that decision.

## And now, Emacs!

First of all let me state this: I am a noob. I am not a seasoned programmer who has
been doing this for decades, sure thing. I am someone who got interested in programming
and had luck to have someone expose me to the advanced tools I surely wouldn't try on
my own. **But *Emacs* is not that hard to learn as most people are often claiming it to be.**
You just need to accept a few things, such as you won't learn to use it properly in a
couple of days. That's because every powerful tool that offers the level of customization
(and freedom to do things you think of) that *Emacs* offers requiers of the user to spend
some time to learn how to use it. But I found process of learning to use *Emacs* really
fun.

What i like about *Emacs*: first of all, I can do all the things only with the keyboard!
I'm serious. I never have to reach for the mouse when you're using *Emacs*. It is
**that** powerful. You may say now "OK, but I don't really mind reaching for the mouse
every now and then". But when you learn touch typing you realize that for the
greatest efficiency your fingers need to be positioned at the home row (you'll learn
all this using aforementioned *Klavaro*). Every time I reach for the mouse it
kind of breaks my typing flow, heck, even my thought process.This tends
to be really frustrating. I never have those kind of problems with *Emacs*.

Second of all: Since I'm currently learning to program in **Python** programming
language (more about that in one of the next blog posts) it is really useful for me to
have all the things in one place: an **editor** to write my code in, and an **Interpreter**
to execute my code. And with *Emacs* I can do that easily. I just write my code, then
I send it to the interpreter with a keyboard shortcut, and it immediately executes it.
Then, if I want to edit something in my code I just do it, repeat the keyboard shortcut
for sending code to interpreter, and voila, it spits out the result. It is that easy.
And since *Emacs* has a great way of managing multiple windows I can look at the
code I'm editing and interpreter at the same time. This is really helpful since
it eliminates the distraction and lets me concentrate on the code I'm writing.

### Prelude

*My favourite Emacs theme, Cyberpunk:*
![Cyberpunk Theme](/cyberpunk-theme.png)

I almost forgot to mention [Prelude](https://github.com/bbatsov/prelude "Prelude GitHub page").
Prelude is an *Emacs* distribution aimed to enhance the default *Emacs* experience.
You see, when you install *Emacs* it looks OK, nothing special, but what Prelude does
is that it alters bunch of default settings and adds a lot of additional packages as
well as its core library. With it *Emacs* is easier to use (at least for me), offers
more power to the more experienced users as well as to newcomers. And last, but not least:
It looks way cooler (default prelude settings use the
[zenburn theme](https://github.com/bbatsov/zenburn-emacs "Zenburn Emacs Theme")
but I think the [cyberpunk theme](https://github.com/n3mo/cyberpunk-theme.el "Cyberpunk Emacs Theme")
is way cooler! If you don't like neither - that's not a problem. There are a lot of
different *Emacs* themes out there.

*Prelude's default theme, Zenburn:* <br>
![Zenburn Theme](/zenburn-theme.png)

I could write about *Emacs* in much greater detail, but in that case this wouldn't
be a blog post. It would be a long book, more like a manual on using *Emacs* and there
are already dozens of great books and manuals covering that topic.

I would appreciate your comments, what you liked, what you didn't like, what could
be better or anything that comes to mind actually.
