---
title:  "In Search of a Good Way to Learn Programming"
date:   2019-07-24 22:20:00 +0200
author: Savo Djuric
categories: [Programming]
tags: [clojure, clojurescript, learning, beginner, programming, book, fulcro]
---

Starting from scratch is hard. Especially if you are taking the "teach-yourself"
approach. This blog post is going to be about what I think, from my beginners
perspective, would be a good approach to learn programming.

### Enclosed approach, not partial

As you may know, I am trying to learn Clojure/ClojureScript (they are almost identical,
as far as I know). There indeed is literature that can teach you the language,
BUT: It seems that all the books focus on describing the language itself. What
seems to be missing, in my opinion, is a book that is written for beginners in programming,
that teach you programming while teaching you the language. *Talk about concurrency, heh*.

### The Struggle

In my [previous post](https://savo.rocks/programming/2019/07/11/clojure(script)-what-do-you-think.html)
I talked about [Clojurscript Unraveled](https://funcool.github.io/clojurescript-unraveled/)
It seems like a good book as a reference to ClojureScript language itself, but to learn
programming? Sadly, no.

A lot of clojurians recommended [Clojure for the brave and true](https://www.braveclojure.com/clojure-for-the-brave-and-true/)
and I gave it a try last time I wanted to learn Clojure (about a year ago). It seems
like an interesting book, with lots of fun examples, but at that time it seemed more like
a book for someone who is already proficient in at least one other programming language.
To be fair, maybe I didn't read it as thoroughly as I maybe should.

### What it seems to me that is good way to learn programming

The conclusion I have reached, after some time is that there will never be a "perfect way"
to learn programming. It appears that the best way is to dabble with a language,
making software from the simplest command-line apps that return a greeting to trying to make
more complex apps. Big part of it I now realize, is to be asking around on internet, hoping that more
experienced folks are willing to help (and clojurians definitely are), writing blog posts and so on.

### So now what?

I definitely decided to read [Clojure for the brave and true](https://www.braveclojure.com/clojure-for-the-brave-and-true/)
again, but I'll take my time to make sure I don't skip chapters until I have a solid understanding of the material presented.

Also, I will try to be more active in terms of writing about my learning progress as
I'm sure that people who have much more experience than I will be able to detect my shortcomings
and steer me on a good path.

Expect a lot of "stupid" questions one true programming beginner might ask.


*Feel free to leave comments, contact me on social networks if you have some useful advice
or just want to connect with me.*
