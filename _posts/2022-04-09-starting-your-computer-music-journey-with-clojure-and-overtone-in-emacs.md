---
title: "Starting your Computer Music Journey with Clojure and Overtone in Emacs"
date: 2022-04-09 21:30:00 +0200
author: Savo Djuric
categories: [Programming, Music, Clojurists Together 2022 Q1]
tags: [clojure, programming, music, overtone, livecoding, emacs, editor]
---

In this post I want to write about probably the most powerful text editor there is: [Emacs](https://opensource.com/resources/what-emacs){:target="_blank"} and how to set it up so you can begin programming and live coding with Overtone in no time. Many users would disagree and argue that [vi](https://en.wikipedia.org/wiki/Vi){:target="_blank"}/[Vim](https://en.wikipedia.org/wiki/Vim_(text_editor)){:target="_blank"} is the king of editors, and I'm not going to get into that bickering, which has lasted for several decades, as can be seen on [this wiki page](https://en.wikipedia.org/wiki/Editor_war){:target="_blank"}. Even Google joined in on this joke, as can be seen in the following screenshot:
![Google Emacs/vi search](/emacs-vs-vi.png)
<br>
I'm just going to say that Emacs has been my primary text editor/programming environment for several years now, and I found that it is very pleasant to use once I got past the fact that it has different keybindings than most modern programs.

## Why should you consider Emacs?

It is one of the oldest editors still in use. It is very stable, powerful, extensible, and has great learning resources. On top of that, there is a mature development environment built for Clojure called [CIDER](https://github.com/clojure-emacs/cider#readme){:target="_blank"} that most Clojurians use. In fact, for every programming language there is a [mode](https://tldp.org/HOWTO/Emacs-Beginner-HOWTO-3.html){:target="_blank"} which enhances your experience while working in Emacs. Modes also exist for other software programmers typically use, such as [Magit](https://github.com/magit/magit#readme){:target="_blank"} for git or [Ansi-term](https://wikemacs.org/wiki/Ansi-term){:target="_blank"} if you want to run a terminal process inside Emacs.
<br>
Since Emacs is keyboard oriented (although you can use it with context menus if you want) in this article everything I do and describe will be done using keybindings. Although harder at first, it ultimately enables us to be more proficient and have better workflow, rarely taking our hands off our keyboards. There are many other benefits to using Emacs, but in favor of keeping this article short(ish) we will leave that for some other time. Now, let's move on to the installation and setup!

## Install Emacs

### Linux
There are many ways to install Emacs on Linux, but the easiest is probably through your distribution package manager. Since I'm using Arch Linux with default package manager Pacman, I can just type in my terminal `sudo pacman -S emacs` and voilà, it is on my system and ready to run. If you are on Ubuntu, the command for you would be `sudo apt install emacs`. Best way to find out is to search the web for installation instructions on your particular distribution. But most of them probably have very similar and easy process as Arch or Ubuntu.

### Mac OSX
I seldom use Apple computers and their OSX, but I came across [this page](https://wikemacs.org/wiki/Installing_Emacs_on_OS_X){:target="_blank"} which shows us several ways how to install Emacs. I know that [Homebrew](https://brew.sh/){:target="_blank"} is pretty popular, so you can use it for installation. I noticed that you should first uninstall outdated version of Emacs that comes with the system and then you can install current version. Execute the following lines in your terminal:
``` shell
sudo rm /usr/bin/emacs
sudo rm -rf /usr/share/emacs
brew tap railwaycat/emacsmacport
brew install emacs-mac
```
and that should do it. If you want to do it another way visit the first link I gave in the Mac OSX section.

### Windows
This operating system doesn't have an integrated package manager, so it's best to go to [GNU Emacs for Windows repository](https://ftp.gnu.org/gnu/emacs/windows/){:target="_blank"}, open the latest directory (at the time of writing of this article that is [emacs-28](https://ftp.gnu.org/gnu/emacs/windows/emacs-28/){:target="_blank"} and download the *emacs-xx.x-installer.exe* (xx.x representing the version number here). Run the downloaded executable and install the program. The same process can be repeated for updates in the future.
<br>
Next, you should set the `HOME` variable because we are going to need it when configuring Emacs. Open Powershell as administrator and execute the following: `setx HOME %USERPROFILE%`. This will set your `HOME` variable location to `C:\users\yourusername\`. You can specify some other location if you wish so.

## Configure Emacs

Plain Emacs installation looks like this:
![Plain Emacs Installation](/emacs-vanilla.png)
<br>
It looks much like any software: It has context menus at the top, then some useful graphical shortcuts below, and the rest is a welcome screen that you get when you first open Emacs. It contains some info on the program and further links where you can learn to use the editor.

### Prelude
Since Emacs has been actively developed for several decades, many people configured it to suit their particular needs. But most of us are lazy and want to change as little as we need to have the best experience possible. Fortunately, there is a project called [Prelude](https://github.com/bbatsov/prelude#readme){:target="_blank"} that enhances Emacs experience and makes it so that you get fully functioning development environment for many programming languages. It also contains [CIDER](https://github.com/clojure-emacs/cider#readme){:target="_blank"} which means we don't have to install it separately. You can go to [Prelude's GitHub Page](https://github.com/bbatsov/prelude#readme){:target="_blank"} to find out more about the changes it brings to the default Emacs installation.
<br>
Prelude replaces your *.emacs.d* directory which is a place where emacs stores all preferences. You can easily install it by executing `curl -L https://git.io/epre | sh` in your terminal. Another way to do it is to visit aforementioned Prelude GitHub page, click on the green *Code* button and then *Download ZIP*. Or you can just use [this link](https://github.com/bbatsov/prelude/archive/refs/heads/master.zip){:target="_blank"} to download that same ZIP. Then extract the contents in your `$HOME/.emacs.d/` directory and the effect should have the same results as if you used the terminal method, except the terminal method backs up your *.emacs.d* directory automatically, and manually downloading and extracting the repo from the ZIP doesn't do that.

### Tweaking Prelude
If you open up Emacs now you'll see bunch of changes happening really fast. This is because Emacs applies new settings which we got with Prelude. After that fairly short process you'll get the message similar to "Prelude is ready to do thy bidding, Master *username*!". Now, we need to do some minor tweaking to enable various prelude modules which will enhance our Experience.
<br>
First, copy the file `prelude-modules.el` from `your-.emacs.d-directory/sample/` to `your-.emacs.d-directory/personal/` and open the freshly copied file in your favorite text editor (yes, I do mean Emacs!). There you can uncomment (remove semicolons before starting parenthesis) for modes that you want to be loaded. Here's how my *preload-modules.el* file looks like:

``` emacs-lisp
;;; Uncomment the modules you'd like to use and restart Prelude afterwards

;;; General productivity tools

(require 'prelude-ido) ;; Supercharges Emacs completion for C-x C-f and more
;; (require 'prelude-ivy) ;; A mighty modern alternative to ido
;; (require 'prelude-selectrum) ;; A powerful, yet simple, alternative to ivy
(require 'prelude-helm) ;; Interface for narrowing and search
;; (require 'prelude-helm-everywhere) ;; Enable Helm everywhere
(require 'prelude-company)
(require 'prelude-key-chord) ;; Binds useful features to key combinations

;;; Vim emulation
;;
;; Enable this module if you're fond of vim's keybindings.
;; (require 'prelude-evil)

;;; Org-mode (a legendary productivity tool that deserves its own category)
;;
;; Org-mode helps you keep TODO lists, notes and more.
(require 'prelude-org)

;;; Programming languages support
;;
;; Modules for a few very common programming languages
;; are enabled by default.

(require 'prelude-c)
(require 'prelude-clojure)
(require 'prelude-coffee)
(require 'prelude-common-lisp)
(require 'prelude-css)
(require 'prelude-dart)
(require 'prelude-emacs-lisp)
(require 'prelude-erlang)
(require 'prelude-elixir)
(require 'prelude-fsharp)
(require 'prelude-go)
(require 'prelude-haskell)
(require 'prelude-js)
(require 'prelude-latex)
(require 'prelude-lisp) ;; Common setup for Lisp-like languages
(require 'prelude-lsp) ;; Base setup for the Language Server Protocol
(require 'prelude-lua)
(require 'prelude-ocaml)
(require 'prelude-perl)
(require 'prelude-python)
(require 'prelude-racket)
(require 'prelude-ruby)
(require 'prelude-rust)
(require 'prelude-scala)
(require 'prelude-scheme)
(require 'prelude-shell)
(require 'prelude-scss)
(require 'prelude-ts)
(require 'prelude-web) ;; Emacs mode for web templates
(require 'prelude-xml)
(require 'prelude-yaml)

;;; Misc
(require 'prelude-erc) ;; A popular Emacs IRC client (useful if you're still into Freenode)

(provide 'prelude-modules)
;;; prelude-modules.el ends here
```

If you compare it to your starting file you'll see that, apart that I enabled all programming modules, I didn't change much from the defaults. The biggest one is that I use [ido](https://www.emacswiki.org/emacs/InteractivelyDoThings){:target="_blank"} completion tool instead of [ivy](https://github.com/abo-abo/swiper){:target="_blank"} or [selectrum](){:target="_blank"}. Although ivy and selectrum appear to be more powerful than ido, I just like the simplicity of it as it does what it has to do and doesn't get in the way, so I just never switched when it stopped being the default.
<br>
Next thing you should do is create a personal emacs configuration file that will contain your specific configuration options. You should create it on the following location: `your-.emacs.d-directory/personal/preload/emacs.el`. You can name it something other than *emacs.el*, but it needs to be in `personal/preload` directory as that will not get overwritten when you [update Prelude](https://prelude.emacsredux.com/en/latest/installation/#updating-prelude){:target="_blank"}, which you should do from time to time. In the following code block you can see contents of my *emacs.el*:

``` emacs-lisp
(disable-theme 'zenburn)
(setq custom-safe-themes t)
(setq prelude-theme 'cyberpunk)
(setq prelude-minimalistic-ui t)
(menu-bar-mode -1)
(scroll-bar-mode -1)
```

The first three lines make sure that Emacs does not use the default [Zenburn](https://github.com/bbatsov/zenburn-emacs){:target="_blank"} theme and loads the [Cyberpunk](https://github.com/n3mo/cyberpunk-theme.el){:target="_blank"} theme which I find more vibrant and easier to work with. It just comes down to personal preference. I suggest you try it, as it is one of the best themes around. Note that you need to install the cyberpunk theme first in Emacs. You do that by going to Emacs, then pressing M-x (this means to press *Alt* button and *x* button at the same time, but i'll explain that later), type `package-install`, press *Enter*, then type `cyberpunk-theme` and press *Enter*. You have now installed cyberpunk theme and it will be loaded if you use first 3 lines from the code above. Next line, `(setq prelude-minimalistic-ui t)` disables line number on the left side of Emacs window. I have that disabled because in the bottom part of Emacs there is information on which line and character are you exactly, so I found I have no use for it. The `menu-bar-mode` and `scroll-bar-mode` are both set to *-1* value, because I don't want to have neither menu-bar (context menus) or scroll bar, since everything can be done with Emacs keybindings. I have consciously made the decision to make myself learn to use Emacs properly, because I saw the potential of powerful keyboard-oriented editor. And you know what? I haven't regretted it once!
<br>
That's about it as far as tweaking goes. It may seem little overwhelming, but it's really just a couple of things I modified over the years of usage to suit my particular needs. You should have no problem setting it up for yourself the same way.

## Emacs Basics

This article is concentrated on setting up Emacs to be used with Clojure and Overtone for live coding. Properly describing various keybindings and concepts would require much longer article. In fact, many books were written on the subject, including O'reilly's [Learning GNU Emacs](https://www.oreilly.com/library/view/learning-gnu-emacs/0596006489/ch01.html){:target="_blank"} which is what I first read when I was getting to know the editor. Now, go to the link supplied and skip to the section named *Emacs Commands*. There you will find excellent explanation of many useful Emacs keybindings. Or you can read that chapter from the beginning if you want to understand more about Emacs.

## Use Emacs for Clojure Programming and Overtone Live Coding

Now, we have everything set up and we can start using Emacs to do some great stuff. Open Emacs and press `C-x C-f` (hold Ctrl all the time, press x, release x, press f, release f, release Ctrl). Now, navigate to `my-overtone-project/src/my-overtone-project/` and open the file *core.clj*. If you don't have Clojure project with Overtone as dependency, learn how to create it from [Overtone: Basic Setup](/posts/overtone-basic-setup/){:target="_blank"} article that I wrote previously.
<br>
With *core.clj* file open use `C-c M-j` keybinding to start CIDER REPL in Emacs. This will create a new window inside the Emacs frame where you opened the file, and after very short time you should see the prompt `my-overtone-project.core> ` waiting for user input. Go ahead and test it with a few simple commands. Press `C-x o` (Hold Ctrl, press x, release both Ctrl and x and press o) to switch to the buffer where REPL is opened. Type `(+ 3 8)` and hit enter. The REPL will return the result and present you with the prompt again, waiting for your next action. Try `(map inc [3 4 5 6 7 8]`. Again, the REPL returns the result and presents you with another input, like in the following code block:

``` clojure
my-overtone-project.core> (+ 3 8)
11
my-overtone-project.core> (map inc [3 4 5 6 7 8])
(4 5 6 7 8 9)
my-overtone-project.core>
```
Now that we are sure our REPL is working let's go to the buffer where our *core.clj* file is opened. Press `C-x o` again and the buffer we're working in will switch. Populate the *core.clj* as follows:

``` clojure
(ns my-overtone-project.core)

(use 'overtone.live)
(use 'overtone.inst.synth)

(defn play [x]
  "Can play either single tone: (play 60), (play :C4) or Whole chords"
  (if (seqable? x)
    (doseq [note x] (overpad note))
    (if (keyword? x)
      (overpad (note x))
      (overpad x))))

(play :C4)
(play (chord :C4 :minor))

(defn melody [notes sleep]
  (if (empty? notes) nil
      (doseq []
        (play (first notes))
        (Thread/sleep sleep)
        (melody (rest notes) sleep))))

(defn vechord [x y]
  (conj (vec (chord x y)) 0))

(doseq []
 (melody (vechord :a3 :minor) 250)
 (melody (vechord :f3 :major) 250)
 (melody (vechord :c4 :major) 250)
 (melody (vechord :g3 :major) 250)
 (melody (vechord :a3 :minor) 250)
 (melody (vechord :f3 :major) 250)
 (melody [48 52 55 0] 250)
 (melody [43 50 55 0] 250))
```

You can copy it from this page and populate your file using Emacs keybindings. First, use `C-x h` to select everything in the *core.clj* buffer, then hit *Backspace* button to erase everything. Now, copy the last code block from this article and use `C-y` to populate the buffer with the code. It really feels effortless to use keybindings once you get used to them. Now, let's walk through the code.
<br>
First line declares our namespace. Namespaces are very important part of Clojure, but not for this particular example, so we won't go into greater detail here. You can find more about namespaces [here](https://clojure.org/guides/learn/namespaces){:target="_blank"}.
<br>
Next, we have calls to `use` which will load everything defined in `overtone.live` and `overtone.inst.synth` namespaces into our namespace. This is not how we should do things in Clojure, as it is best to use `require`, but we'll proceed for the sake of simplicity in this article. You can see the basic difference [here](https://stackoverflow.com/questions/871997/difference-between-use-and-require){:target="_blank"}.
<br>
Position your cursor at the end of the first `use` call, just after the parenthesis and hit `C-x C-e`. Your REPL should respond by loading Overtone. When it's done remember to connect Overtone audio output to your sound card so you can get sound. The process is described [in one of my previous articles](/posts/overtone-basic-setup/){:target="_blank"}. Move to the end of next line and do the same, and you loaded the synth library from Overtone from which we're going to use `overpad` synth.
<br>
In the next code block we defined the `play` function which will either play a single note or a sequence of notes at the same time. Position the cursor after the last parenthesis in `(overpad x))))` line and execute it with `C-x C-e`, as always. The function is now loaded in the REPL and we can try it out. Execute the `(play :C4)` line and you should hear a cool sounding synth playing the note C4. You can execute this line as many times as you like, should you want to hear that sound again. Let's move along and execute the next line, `(play (chord :C4 :minor))` and we should get somewhat richer, sadder sound. That's because we gave our *play* function three notes to play at once, which together make up the C4 Minor chord. Don't worry about music theory now, this is going to be covered in future articles.
<br>
Let's move on to the next code block, which defines the function named `melody`. This function takes a sequence of notes as one argument, and the time in milliseconds which the thread waits to 'play' the next note. This solution is pretty "hacky" at best, but it's good enough for us to test basic functionality and to play something.
<br>
Next function, `vechord` turns every collection of chords to [vector](https://clojure.org/reference/data_structures#Vectors){:target="_blank"} and adds zero to the end. Adding zero was my hacky solution to get every note at the end of the chord to appear to sound two times longer than the previous two, creating rhythmically pleasant experience (although zero actually plays very low-pitched note that is almost inaudible). I could come up with more elegant solution, but I wanted to leave this at it was to show that imperfection can still sound good! Don't worry too much about it and just execute this expression like you did everything else.
<br>
Finally, we'll execute the `doseq` code block by placing the cursor at the end of the last line and using our familiar keybinding `C-x C-e`. What you hear should be a simple, yet nice melody!

## Summary

Well, here we are, at the end of our article. Although a bit lengthy, it shows in a detailed way:
- How to install and setup Emacs, arguably the most powerful text editor/development environment in the world
- How to tweak the environment so you can truly concentrate on programming in a keyboard-focused way
- How to use Clojure and Overtone to create music with it

Now that we have our environment ready we can focus on exploring what kind of interesting sounds we can create with Overtone and how we can improve it. But more on that topic in the future articles. Until then, try to modify the code from this article and see (or better, listen) to what kind of results you will get. Until next time, happy coding!
