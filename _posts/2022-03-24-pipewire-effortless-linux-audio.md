---
title: "Pipewire: Effortless Linux Audio"
date: 2022-03-24 21:00:00 +0100
author: Savo Djuric
categories: [Programming, Music, Clojurists Together 2022 Q1]
tags: [clojure, programming, music, sonic-pi, overtone]
---
## Live Coding Software

Part of my audio experience on linux is Live coding with software such as [Overtone](https://github.com/overtone/overtone){:target="_blank"} and [Sonic-Pi](https://sonic-pi.net/){:target="_blank"} since I'm very interested in exploring what can be done when two creative fields, programming and music, are combined. Sonic-pi is more user-friendly, has its own environment, works out of the box while overtone is a Clojure project where user is expected to handle everything separately. Since I've been using Emacs for several years that setup was very easy. One thing that Overtone lacks though is a comprehensive guide on how to get from first sounds to a composed piece of music. I aim to change that by exploring Sonic-Pi guide and trying to make something similar for Overtone, as much as it is possible. That effort is sponsored by [Clojurists Together](https://www.clojuriststogether.org/){:target="_blank"}, an organization that supports Clojure developers that create open-source software.
<br>
I decided to write a series of blogs documenting my journey of exploring Overtone possibilities and how much it differs from more mature Sonic-Pi.

## Prelude: Audio Setup

I have been using [PulseAudio](https://www.freedesktop.org/wiki/Software/PulseAudio/){:target="_blank"} and [JACK](https://jackaudio.org/){:target="_blank"} since I switched to Linux long time ago. The combination has been working fine most of the time. PulseAudio was handling consumer audio tasks (playing audio from web browser, music player apps, etc) and JACK took over for apps that required low-latency (Audio Recording apps such as Ardour, Reaper and such). I managed to set-up parameters for JACK so as not to have any noticeable latency when recording, but the process was not that easy at first, and there were still some issues when using VST audio plugins made for windows (the latency was not a problem, but there was a lot of crackling which made recording experience unpleasant). Since there wasn't any alternative to this setup I learned to tweak it as best as I can and to live with it.


## Pipewire: a new player in Linux Audio field

When I recently updated my system I noticed something strange: Sonic-Pi could not start. The welcome screen would freeze and after some time the message would pop-up saying that audio server could not be started. When I looked it up online I found out that several people had similar problem but all the solutions were kinda hacky and none worked for me. Somewhere one user mentioned that installing `pipewire-jack` package solved it for him and I decided to find out what that is. As it turns out, [PipeWire](https://pipewire.org/){:target="_blank"} is a relatively new project that aims to replace both PulseAudio and JACK for Linux audio. In fact, Fedora, one of the better-known linux distributions out there is already using PipeWire as default system audio server. I decided to try it out and fired up my terminal. Since I'm using Arch Linux, my package manager by default is `pacman`. Arch Wiki suggested to install the packages `pipewire`, `pipewire-alsa`, `pipewire-pulse` and `pipewire-jack` to be able to use PipeWire for managing all audio streams on the system. That turned to be not that simple because lots of packages had pulseaudio or jack as dependency and I needed to uninstall some packages first, then remove pulseaudio and jack, install pipewire packages, and then finally install the packages I removed before switching due to dependency issues.

## Easier than expected

I was prepared to dive into a long process of configuring and tweaking audio settings to get everything to work right, remembering the experience of previous audio configuration process. First of all, I set the buffer size in the appropriate config file (detailed information can be found on [PipeWire Arch Wiki Page](https://wiki.archlinux.org/title/PipeWire){:target="_blank"} and started the necessary systemd services. I tested consumer sound (browser, audio/video file player) - it worked fine. Next, I fired up audio recording software and tried recording my guitar that was plugged in to my sound card - that worked excellent. Latency was unnoticeable and there were no interruptions. I immediately fired up guitar amp windows VST plugin that I have and like (the one I mentioned above that had crackling issues). I started playing and to my amazement - the sound was perfect! No crackling, no latency! I couldn't believe it was that easy, everything worked out of the box.

## Sonic-Pi: Temporary Solution

I had hoped that switching my audio framework will solve my problems with Sonic-Pi, but I was not that lucky - the problem was still the same. I decided to install [Sonic-Pi from flathub](https://flathub.org/apps/details/net.sonic_pi.SonicPi){:target="_blank"} until I figure out how to solve a problem that my main installation has. I first installed `flatpak` package from arch package repository, then downloaded install file from flathub link and installed the package with `flatpak install /path/to/where/i/downloaded/the/file` command. Then I made the alias to starting sonic-pi through flatpak since there was no way I would type `flatpak run net.sonic_pi.SonicPi` every time I wanted to start the program. Finally, everything worked fine, both program and the audio are working as expected and I can get on to make some interesting pieces of music.

## What's next?

This article is just a short description of my transition from one linux audio framework to another and announcement of future blog posts where I would be comparing the differences between sonic-pi and overtone live coding software. I understand that I used some terms in this article and didn't quite explain them, but I'll leave that for future articles where I'll go into more detailed explanation. If you have any questions or suggestions I'd be happy to hear them. Until next time. Happy coding!
