---
title:  "Clojure(Script): What do you think?"
date:   2019-07-12 00:00:00 +0200
author: Savo Djuric
categories: [Programming]
tags: [clojure, clojurescript, learning, beginner, programming, book]
---

### A Two-year's pause

If you check out the gap between this and the previous article I published you'll
notice that it's more than two years difference. The truth is that job insecurity
didn't leave me enough time to focus on learning long-term, so I didn't think about
learning programming or writing blog posts for quite some time. But that's changed.

### Starting again

After doing bunch of dead-end, short-term jobs I landed relatively stress-free
position working in IT application & network department at one regional bank in my country
that isn't going away anytime soon. And with that came freedom to spend my free time
focusing on programming.

### Why Clojure(Script) & Literature I chose

Well, I wanted to learn Clojure for quite some time now, because I always heard praises about how
simple and powerful the language is. Community also seems to be great, with lot of people who are
experienced programmers, but are always willing to discuss the difficulties that beginners face during
their struggle to learn Clojure. As for the language itself, it is awesome that you can
develop code in real-time, thanks to REPL, which makes process less disturbing and
allows the developer to focus more on the program design itself.

Also, the code seems to be very clean, meaning that there are no long blocks of code
(like JavaScript, for example), where half the lines are just {, }, (, ), [, ],
and of course, this guy: ;

*Interesting thing is that many people are put-off by languages from lisp family,
because, as they say "There are too many Parentheses". It always struck me as odd,
'cause, even though there sometimes can be couple of parens stacked at the end of
a code block, JavaScript can have even more, they're just spread with newline.*

Another reason is that I'm using Emacs as my default text-editor and IDE (when I'm trying to learn programming)
and one great Clojurian named [Bozhidar Batsov](https://twitter.com/bbatsov) has
made an excellent add-on, [CIDER](https://github.com/clojure-emacs/cider), which is an excellent Clojure
development environment. Also, I've been using [Prelude](https://github.com/bbatsov/prelude)
from the same author.

*Prelude is Batsov's enhanced Emacs setup which makes Emacs
experience much more pleasant and powerful.*

#### ClojureScript Unraveled

I wanted to start with material that's leaning more towards beginners (yes, I know
about [Clojure for The Brave and True](https://www.braveclojure.com/), but
I wanted to try something that's even more beginner friendly, and I think I found it
The book is called [ClojureScript Unraveled](https://funcool.github.io/clojurescript-unraveled/),
and it's focused on ClojureScript (which is, as I understand, very similar to Clojure, unlike
Java and Javascript :D). Since Web Development is beginner-friendly, I decided to try this book
to try to learn basics of Clojure(Script) and how they're applied in the real-world.
Once I finish [ClojureScript Unraveled](https://funcool.github.io/clojurescript-unraveled/) I will
certainly give [Clojure for The Brave and True](https://www.braveclojure.com/) a chance.

### The purpose of this blog post

I plan to frequently publish blog posts about my learning progress, and I hope
that enough people will enjoy reading them, helping me out when I'm stuck on something,
giving me useful advice, or maybe just recall their beginnings when they struggled
to understand powerful, yet not-so-easy to grasp as a language when you have no experience.

*Please leave a comment if you have any suggestions, maybe what other books you know that are good
for beginners, what book could I start reading when I finish the one that I'm currently doing or any
other useful comment in general. Thanks for reading.*
