---
title:  "Jekyll setup in NixOS"
date:   2017-01-31 00:10:00 +0100
author: Savo Djuric
categories: [Programming]
tags: [jekyll, nixos, tutorial, github]

---

Hello people. This is my first blog post, and the topic I picked out to talk about
is how to setup your Jekyll blog on NixOS, because that's where I had some trouble.
I promised myself that I would write about this as soon as I get it running on my
configuration.

---

#### The short version (tl;dr):
With **Jekyll** you generate your site/blog locally, and serve it (host it locally
on your machine (usually on localhost:4000 address) where you can see changes in
real-time as you edit your blog posts). With **Markdown** you write content
(much easier than HTML) which Jekyll then generates into HTML. Use **Git** to
upload ('push' in git terms) your blog, when finished, to **Github Pages**,
which is there for hosting purposes.

**P.S.** If you one day decide you want to migrate your blog to a hosting service
other than **Github Pages** you can easily do that with **Git**.

### And if you're really interested:

First of all, you might wonder "What is Jekyll?". Well, in a couple of sentences,
Jekyll is a simple, blog-aware, static site generator for personal, project, or
organization sites. In the words of its authors:

>*it does what you tell it to do, no more, no less. It doesn't try to outsmart users
by making bold assumptions, nor does it burden them with needless complexity and configuration.
Put simply, Jekyll gets out of your way and allows you to concentrate on what truly matters: your content.*

Content for Jekyll is written in **Markdown**, which is a
lightweight markup language with plain text formatting syntax designed so that
it can be converted to HTML. That's what Jekyll is for: it takes your content writen in
Markdown, and converts it to HTML.
[Get familiar with Markdown. It's very cool & easy!](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet "Markdown cheatsheet")

#### Why do I use Jekyll with Markdown instead of e.g. Wordpress?
Well first of all Jekyll is lightweight. Unlike Wordpress it's not bloated with
tons of features that you don't need (for a blog, that is). And since Wordpress
relies on dynamic code and database calls, it is slow, and struggles under heavy
load.
And the advantage of Jekyll and Markdown is that you can use whatever text editor
you want to write your content (I use Emacs, but I will explain why in another blog post),
as opposed to Wordpress where you must use the editor they provide.

#### Git
Git is a version control system that you need for easy blog updating.
You use it in your system terminal. If you ask *"Yeah, but what's the point?"*
The answer is quite simple. Imagine every time you want to change something on your blog,
be it the paragraph, picture, or some other info. You would have to go online, edit your files,
re-upload them, etc... all by hand. With git you can easily do this with a couple
commands. See links below on how to use git. Belive me, it's worth it!

#### Github & Github Pages
Github is a git repository hosting service. It is the largest host of source code
in the world. It also offers **Github Pages**, a static hosting site service.
Lots of people that use Jekyll host their blogs on this service.

#### Ruby, RubyGems & Bundler
I will write only briefly about these since you don't need to know in-depth what
they do. **Ruby** is a programming language that Jekyll is written in.
**RubyGems** is a package manager for the Ruby programming language that provides
a standard format for distributing Ruby programs and libraries (in a self-contained
format called a "gem"), a tool designed to easily manage the installation of gems.
And **Bundler** provides a consistent environment for Ruby projects by tracking
and installing the exact gems and versions that are needed.

**Below are some links if you want to further explore topics mentioned above:**

[Jekyll Docs](https://jekyllrb.com/docs/home/ "Official Jekyll Documentation")

[Good Jekyll Tutorial, although for OSX users](https://www.taniarascia.com/make-a-static-website-with-jekyll/ "Jekyll OSX tutorial")

[Git tutorial (for easy blog maintenance)](https://www.taniarascia.com/getting-started-with-git/ "Git Tutorial")

[How to Install Jekyll on Ubuntu Linux](http://jekyll.tips/jekyll-casts/install-jekyll-on-linux/ "Tutorial for installing Jekyll on Ubuntu")


*Don't hesitate to open links for OSX tutorial, because the differences with linux are minimal and it is a very good tutorial*

---

So now that you got yourself familiar with *Jekyll* & *Git/Github/Github pages*
I want to talk about setting it up on NixOS.

#### And what is NixOS?
NixOS is a GNU/Linux operating system that uses declarative system configuration.
What it basically means is that you can experiment more freely with your system
configuration and not be afraid that it'll break, and that you will have to
install your system from scratch all-over again (Windows users, like I was until
recently know what I'm talkin' about). With NixOS the whole system configuration is specified
in one file called *configuration.nix* and that's it. I will maybe write about
this excellent operating system more in the future, but for now I want to stay
on topic. If you want to read about NixOS right away I suggest that you start
from the [NixOS official site](https://nixos.org "Official Nixos website").
This is also a good link to learn more
[About NixOS](https://nixos.org/nixos/about.html "NixOS Official about page")

---

All right. First of all make sure that your NixOS is up to date. It's important
because you may run into some errors if you're using older versions of ruby and bundler.
To check the versions type in terminal `ruby -v` and `bundler -v` (As I write this my
Ruby version is 2.3.1p112 and bundler 1.13.6. That can change by the time you read this).
We need the latest versions of both so make sure that you switch to the:
1. Unstable channel or
2. Latest stable version channel

[More about NixOS channels & upgrade](https://nixos.org/nixos/manual/index.html#sec-upgrading "NixOS channels & upgrade")

Let's say you want to be on *unstable* channel. **Both globally and locally** (for specific user).
Open the *terminal* and type the following:

1. Local channel: `nix-channel --list | grep nixos`
2. Global channel: `sudo nix-channel --list | grep nixos`

If it says that you are on the unstable channel i.e.

`nixos https://nixos.org/channels/nixos-unstable`

then you're fine. If not, run the following from your terminal:

1. Locally: `nix-channel --add https://nixos.org/channels/nixos-unstable nixos`
2. Globally: `sudo nix-channel --add https://nixos.org/channels/nixos-unstable nixos`

*I am only emphasising this because it was confusing for me at the beginning and I
thought that I was on the unstable channel globally, when in fact I was on an older channel.
If I was confused I'm going to make sure that you're not. And that you won't repeat my mistakes.*

The packages you need for Jekyll to run are:
- jekyll
- gitAndTools.gitFull
- bundler
- bundix

You need to add abovementioned packages to your *configuration.nix* file.
More about this on [NixOS Installation tutorial](https://nixos.org/nixos/manual/index.html#ch-configuration "NixOS Configuration Tutorial").
BTW you can search for packages at [NixOS list of packages](https://nixos.org/nixos/packages.html "NixOS Packages").

After that you just run:
`sudo nixos-rebuild switch`
and your system is now updated. Restart is not necessary, but go ahead if you feel like it.

---

Now we get to the point of this post. Your Jekyll blog needs Ruby Gems for lots
of its features to work, but due to the specific NixOS package management it can't work.
This is where **bundix** comes in. As the package description says, *bundix* creates Nix packages from Gemfiles.

What you want to do next is, in terminal, change directory to your local jekyll blog and run the following:<br/>
`bundler lock`<br/>
`bundler package --path vendor/cache --no-install`<br/>
`bundix`<br/>
This will generate *gemset.nix* file that is going to be used by the *default.nix* file.
Go ahead and create the *default.nix* file in the same directory and put following in it:

```nix
with (import <nixpkgs> {});
let
  env = bundlerEnv {
    name = "your-package";
    inherit ruby;
    gemfile = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset = ./gemset.nix;
  };
in stdenv.mkDerivation {
  name = "your-package";
  buildInputs = [env ruby];
}
```

[Here you can learn more about bundix](https://github.com/manveru/bundix/ "Bundix")

Now all that's left is to run<br/>
`nix-shell default.nix`<br/>
In your Jekyll blog folder and then in the prompt type:<br/>
`jekyll serve`<br/>
And now you're all set. Everything should work as expected.

---

That's it for this post, I hope that everything is clear and that you'll have your
Jekyll setup in no time. Happy blogging.
