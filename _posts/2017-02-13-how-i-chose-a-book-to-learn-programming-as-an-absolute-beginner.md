---
title:  "How I Chose a Book to Learn Programming as an Absolute Beginner"
date:   2017-02-13 22:30:00 +0100
author: Savo Djuric
categories: [Programming]
tags: [python, nixos, tutorial, beginner, programming, book]

---

Who would think that finding the right book to learn programming from scratch can be hard?
Certainly not me. But as I began to learn to program from various books I constantly found myself giving up after
several chapters because the more I worked through those books the less I understood. Until I found the book that
I stuck with. The book was [The Coder's Apprentice: Learning Programming with Python 3](http://www.spronck.net/pythonbook/).

I choose this book because it doesn't try to cram up too much stuff in the beginning and get you
to make a game for example in the first 3 or 4 chapters. It concentrates on teaching the reader
the basics of programming from the simplests steps and then moves on to more advanced stuff.
In the following text I'll write about my prior experiences with programming, why I kept failing to
stick with it and in the end, why I chose the aforementioned book, why I like it and why I think this book
is the first one you should read if you want to learn programming without prior experience.

### A short backstory

So for some time I wanted to learn programming. I didn't have any experience with it, except I had
some classes in high school where we learned C++, but I didn't really learn much there, and I quickly
forgot what I learned as soon as the course was done. At that time I wasn't really thinking about
learning to program.

Shortly after high school (I didn't enroll in college) my brother suggested a
couple of books he though were good choice to start learning programming as in the meantime
I developed some interest in the topic doing little ActionScript (read about it [here](https://goo.gl/AoCWJL)).
As he is a seasoned programmer, he would guide me and help me through the process. One of the books
was Miran Lipovaca's [Learn You a Haskell for Great Good! (A Beginner's Guide)](http://learnyouahaskell.com/)
and the other one was
[Practical Clojure](https://www.amazon.com/Practical-Clojure-Experts-Voice-Source/dp/1430272317/).

I started learning from both books, but struggled heavily because I didn't understand very much and
quickly lost interest. To be fair, I was a lot less patient back then, and I realize now that those
books were too advanced for someone who has absolutely no experience. They are a great choice for someone
who already has experience in other languages and wants to dive into Haskell and Clojure. That was back in 2013/14.
After this failed attempt I concentrated on other things and forgot about programming for some time.

Then, as the end of 2015. was approaching I wanted to make a site for my band (Oh yeah, I am a musician),
so I started learning HTML/CSS from the book called [Learning Web Design](http://www.learningwebdesign.com/)
I began working on my site as I progressed through the book. Then after some time I learned that it's better
to implement bootstrap if you don't want to do some serious HTML/CSS, since you need to be experienced with those
if you want to make something better than can work better for you than bootstrap. And I didn't really care for
HTML/CSS...

### Choosing the 'right' language to learn programming

After that I decided to pick up programming again, in August or September of 2016 (actually,
I tried some of the 'beginner' programming books in the meantime, but I cannot write about all
of them in this post if I don't want it to be too long).
This time I decided to search the web to find out which is the most recommended language
for a beginner. In the meantime I noticed that a lot of people suggest Python, so it was
no surprise to me to see that wherever i looked about 70-80% of the posts were saying
'python'.

Cool. Python. Now I had to find a book and start learning. I decided to look through various recommendations,
read reviews etc. because I wanted to pick up a book that I'm going to stick with. After some searching I
found the book that seemed just right. I think it was on PythonWiki, under the section about Python Books for
Beginners. The book was
[The Coder's Apprentice: Learning Programming with Python 3](http://www.spronck.net/pythonbook/).

I cannot recommend this book enough. Why I liked this book:

1. It is written for **absolute** beginners, who have *absolutely* no prior experience
2. Rather than *'teaching you to build your own game in the first 3-4 chapters'* (which a lot of books do)
it starts from the simplest stuff and moves on to more advanced things. This way it takes more time to feel
you're actually doing something, but it's the right way.
3. At the end of every chapter there are several exercises where you can test if you understood the topic.
Exercises are good, the only required knowledge is from the already finished chapters.
4. This book is absolutely free. Just go [here](http://www.spronck.net/pythonbook/), download it and
start learning.

### For The End

This is the first programming book that didn't feel completely strange to me, I found the material
easy to follow, exercises were good, for some of them I had to spend hours to do and it paid of.
I finished about 2/3 of the book and really understood everything.

*P.S. Please leave a comment if you have any suggestions, maybe what other books you know that are good
for beginners, what book could I start reading when I finish the one that I'm currently doing or any
other useful comment in general. Thanks for reading.*
