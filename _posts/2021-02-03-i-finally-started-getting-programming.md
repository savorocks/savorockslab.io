---
Title:   "I Finally Started Getting Programming"
date:    2021-02-03 22:05:00 +0100
author:  Savo Djuric
categories: [Programming]
tags: [clojure, luminus, web development, learning, book]
---

For a few last years I've been learning programming in various languages, on and off, with more or less success.
Some languages that I've tried include Python, Java, Haskell and even JavaScript. And of course Clojure.
But it has been going slowly, not because those languages (or programming in general) are hard. It's
because I lacked consistency in doing things on regular basis due to lack of time and energy; I've often
been mentally exhausted at my regular IT work. Here's how I finally managed to organize myself and actually
started getting it!

### Ancient history

I've been interested in computers for most of my life. It can be shortly described
as 'power user' e.g. *system configuration, setup, various audio/video editing tools...*, but I've never really
took up programming until 2017. (You can read about my contemporary views [here](/posts/an-unexpected-toolbox-for-a-beginner-programmer/){:target="_blank"}).
Back then I managed to learn some Python syntax, but it all came down to following the materials I have been
learning from, and I gave up after some time. In the meantime I have been dabbling with various languages
and technologies, but never crossed the chasm to serious work beyond tutorials and books.

### Recent development

When I wasn't trying to learn programming I was playing guitar with various bands I had and worked in several
companies on various positions in IT departments. Those positions included various tasks such as system administration
& maintenance, vendor coordination and even IT support call-center coordination. During that time I still managed
to dabble with programming irregularly, mostly with Clojure because I liked how it doesn't get in my way.
It can be set up almost instantly and the syntax is very minimal, which lets me concentrate on shaping my
ideas into code. But very often the workload was big enough to keep me from programming constantly, so I didn't
get very far. However, recently I decided to concentrate on programming, as I realized that it brings me much
joy, even when I am completely lost in the process, because when I overcome obstacles it feels phenomenal.

### Current situation

I discovered a one-step-beyond-basics book that suits my needs perfectly: [Web Development with Clojure](https://pragprog.com/titles/dswdcloj3){:target="_blank"}
and bought a copy. I'm getting serious now :) Currently, the book is in beta, but it is expected to be finished very soon.
This book shows in detail how to build modern web applications using various Clojure libraries. Clojure
is different from other languages in that matter: it doesn't have a *web-app framework*. Instead, the
programmers combines various libraries which suit their particular needs.

### The approach

So, how is this time different than any other when I tried to learn programming? Read on.<br>
- Walk the unwalked path: This time, instead of just following the book, I decided to explore the
documentation of libraries that were introduced, and to try to experiment with them myself. There are often multiple
libraries that serve the same purpose, and the author is kind enough to mention them in his book. One such example
is two libraries for HTML templating: [Selmer](https://github.com/yogthos/Selmer){:target="_blank"} which
uses Django-like syntax, and [Hiccup](https://github.com/weavejester/hiccup){:target="_blank"} which converts
Clojure vectors to html, so `[:h1 "Hello There"]` becomes `<h1>Hello There</h1>`. Hiccup-way seemed more
attractive to me, so I decided to write the whole template in in, and I have learned more doing that, than I have ever learned following book-code. I was forced to try various approaches out, read the documentation, ask around online for solutions and in the end I felt I understand what I wrote much better.
- Do things regularly: There is a wise saying which goes something like: *it is better to perform average every day, than to be excellent once in a while*. OK, it probably sounds better than I wrote it, but the point is there. I try to follow it, and to program every day for 6-8 hours. I realized that sometimes can be difficult to even start, so I start my programming day by doing <u>one</u> [4clojure](https://www.4clojure.com/){:target="_blank"} exercise per day. This helps me to get *warmed-up* as I can start solving the problem immediately, because the tasks usually require just using the right Clojure function (or combination of functions), so I make sure to search this awesome [Clojure Cheatsheet](http://jafingerhut.github.io/cheatsheet/clojuredocs/cheatsheet-tiptip-cdocs-summary.html){:target="_blank"} for the functions that I'll use, and usually manage to find them. After that I just open my project, the book and code away!
- Revisit previous chapters: I like to come back a little after some time to re-read previous materials to see if I'll have new insight and maybe notice something that I missed the first time when I read the chapter. Usually there are *a-ha* moments, and makes this part very valuable in the process.

The book guides reader through building a microblogging platform, kind of like a social network, where multiple users can have their message boards and interact with each other, have their feeds, etc. The first few chapters deal with crash-course setting up a page where all the messages are displayed (no multiple users), and the form for entering a message is present, then it moves on to teach about core components of a web app (creating web server, request routing, handling, middleware...), another chapter deals with what [Luminus](https://luminusweb.com/){:target="_blank"} templating library provides, then it moves on to using ClojureScript for SPA approach, which teaches very powerful and modern concepts.
I decided to build something like a *beer-log* application, where I will keep track of craft beers that I've tried. I wanted to do something different, rather than following book paren-for-paren. Who knows, maybe my beer app will someday be good as, I don't know, [Untappd](https://untappd.com/){:target="_blank"} maybe?
