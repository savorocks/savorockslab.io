---
title: "Overtone: Basic Setup"
date: 2022-03-28 18:30:00 +0200
author: Savo Djuric
categories: [Programming, Music, Clojurists Together 2022 Q1]
tags: [clojure, programming, music, overtone, livecoding]
---

## Overtone: Live Coding Software

If you haven't come across [Overtone](https://github.com/overtone/overtone){:target="_blank"} yet, it is a live coding software that enables you to create and control sound using [Clojure](https://clojure.org/){:target="_blank"} programming language. Today we're going to take a look at the process of setting up the project so that you can start experimenting with it. Although setup may be easy for more experienced programmers, it could be quite difficult to some out there who don't have as much experience. Don't worry, after reading this post you should be able to create a project where you can experiment with Overtone successfully.

## Prerequisites

Before we begin make sure that you have the following installed:
- Java (Clojure runs on top of Java Virtual Machine, so it is necessary that you have it). You probably already have this, if not, visit [this link](https://clojure.org/guides/getting_started){:target="_blank"} for more info.
- [Leiningen](https://leiningen.org/){:target="_blank"}, which is a Clojure project management tool. This is not necessary, but it eases the process of managing Clojure projects a lot. It is de facto the default tool. Leiningen has Clojure as a dependency, so your package manager should install it automatically.
- If you're on linux, make sure you either installed [JACK](https://jackaudio.org/){:target="_blank"} or [PipeWire](https://pipewire.org/){:target="_blank"} to handle audio processing. I wrote in [my previous blog post](/posts/pipewire-effortless-linux-audio/){:target="_blank"} about the differences, and why I recommend PipeWire. According to [Overtone Wiki](https://github.com/overtone/overtone/wiki/Connecting-scsynth){:target="_blank"} no specific audio setup is required for Windows or MacOS operating systems.
- If you choose PipeWire to handle your audio, make sure you install a package containing `jack_lsp` cli program. Without it you would get an error when you try to use Overtone. In my case, on Arch linux, it's the [jack-example-tools](https://archlinux.org/packages/extra/x86_64/jack-example-tools/){:target="_blank"}, on Fedora it's called `jack-audio-connection-kit-example-clients`. If your head is already spinning from all this, just use JACK on Linux or Windows/Mac OS.
- A patchbay GUI program to easily connect various audio sources to playback devices. If you have PipeWire install [qpwgraph](https://gitlab.freedesktop.org/rncbc/qpwgraph){:target="_blank"}, if you have JACK installled [carla](https://kx.studio/Applications:Catia){:target="_blank"} is a good choice. You'll see at the end of this article why we need this.

## Installation and Setup

Now that we have everything we need to get started, fire up your terminal and type `lein new my-overtone-project` which will create a new directory with necessary files for basic Clojure project. In your text editor of choice open the _project.clj_ file that is located in the root of newly created _my-overtone-project_ directory. You will see several lines that are defining the project (for description on what they do read the [Leiningen tutorial](https://github.com/technomancy/leiningen/blob/master/doc/TUTORIAL.md){:target="_blank"}). The line we're interested in starts with `:dependencies`. It should look like this:

``` clojure
:dependencies [[org.clojure/clojure "1.10.3"]]
```

The Clojure version will be set by Leiningen when you create the project, so it can differ from what you see in this article depending on when you're reading it. Now, edit the `:dependencies` section as follows:

``` clojure
:dependencies [[org.clojure/clojure "1.10.3"]
               [overtone "0.10.6"]]
```

What we've done here is we added overtone package as a dependency of our project, which means that Leiningen will take care for us to download it from [Clojars](https://clojars.org/){:target="_blank"} and include it in our project. Next, go back to your terminal, change directory to the root of your project _my-overtone-project_ and type `lein repl`. With that command Leiningen will first download all the dependencies that are needed, and then it will start a [Clojure REPL](https://clojure.org/guides/repl/introduction){:target="_blank"} (see the link for more info about what REPL is). When the process is done you should see the prompt `my-overtone-project.core=> ` which means that everything went OK and that REPL is waiting for your input. Test if Clojure works with some of the basic fuctions, for example:

``` clojure
my-overtone-project.core=> (+ 3 8)
11
my-overtone-project.core=> (map inc [2 3 4 5 6])
(3 4 5 6 7)
my-overtone-project.core=>
```
Cool! We get the expected output from the functions called. Now, let's try if Overtone works. In the REPL, type `(use 'overtone.live)`. If evertyhing is allright you should get a message similar to this:

``` clojure
my-overtone-project.core=> (use 'overtone.live)
--> Loading Overtone...
Mar 28, 2022 5:12:50 PM clojure.lang.Reflector invokeMatchingMethod
WARNING: Can't listen to midi device: {:description "Scarlett 2i4 USB, USB MIDI, Scarlett 2i4 USB", :vendor "ALSA (http://www.alsa-project.org)", :sinks 0, :sources 2147483647, :name "USB [hw:0,0,0]", :overtone.studio.midi/full-device-key [:midi-device "ALSA (http://www.alsa-project.org)" "USB [hw:0,0,0]" "Scarlett 2i4 USB, USB MIDI, Scarlett 2i4 USB" 0], :info #object[com.sun.media.sound.MidiInDeviceProvider$MidiInDeviceInfo 0x1051cd0a "USB [hw:0,0,0]"], :overtone.studio.midi/dev-num 0, :device #object[com.sun.media.sound.MidiInDevice 0x28f753fc "com.sun.media.sound.MidiInDevice@28f753fc"], :version "5.16.15-arch1-1"}
javax.sound.midi.MidiUnavailableException: No such device
  **********************************************************
       WARNING: JVM argument TieredStopAtLevel=1 is active, and may
       lead to reduced performance. This happens to currently be the
       default lein setting:

       https://github.com/technomancy/leiningen/pull/1230

       If you didn't intend this JVM arg to be specified, you can turn
       it off in your project.clj file or your global
       ~/.lein/profiles.clj file by adding the key-val

       :jvm-opts ^:replace []
       **********************************************************
--> Booting internal SuperCollider server...
JackDriver: client name is 'Overtone'
SC_AudioDriver: sample rate = 48000.000000, driver's block size = 256
--> Connecting to internal SuperCollider server...
--> Connection established

    _____                 __
   / __  /_  _____  _____/ /_____  ____  ___
  / / / / | / / _ \/ ___/ __/ __ \/ __ \/ _ \
 / /_/ /| |/ /  __/ /  / /_/ /_/ / / / /  __/
 \____/ |___/\___/_/   \__/\____/_/ /_/\___/

   Collaborative Programmable Music. v0.10.6


Hello Savo. Do you feel it? I do. Creativity is rushing through your veins today!

nil
my-overtone-project.core=>
```

It started successfully! Don't worry about the warnings at the first half of this snippet (can't listen to midi device and JVM argument one). It is normal if you didn't spend time setting up some other parameters, and it does not prevent us from starting our exploration path with Overtone, so we'll ignore it for now. The important things start at `--> Booting internal SuperCollider server...` line. If everything after that is without errors you should be fine. One more thing before we try to produce some sounds: Overtone doesn't connect it's inputs or outputs to available sound devices automatically, so we need to do it manually. The easiest way is to do this through our patchbay app. In my case, it's _qpwgraph_, and when I open it up i can see that none of my Overtone ins or outs are connected.

*Pathcbay Disconnected:*
![Patchbay Disconnected](/patchbay-01.png)

The visual representation of your patchbay could be a little different, but it will be similar enough so you can manage to connect overtone with your audio card. Now, I can just hover over *out_1* in Overtone section with my mouse, left-click-and-drag to my main audio device left channel (in my case that's *Scarlett 2i4 USB:playback_FL*). Now, the same method should be aplied to *out_2* and it should be connected to right channel of the audio device.

*Pathcbay Connected:*
![Patchbay Connected](/patchbay-02.png)

## Try Overtone!

We are finally ready to test if our overtone setup works. Go back to the terminal window where you previously started the REPL, and type the following `(demo (saw 440))` and press enter.
<br>
Whoa! Did you hear that? That buzzing sound was created by the short code above. The `saw 440` function call is responsible for creating that type of sound (called _saw_, we'll see in future posts why) with the frequency of 440Hz. The demo function is responsible for playing the saw sound we defined. Now, let's make something little cooler. How about playing multiple frequencies at the same time? Type the following in the same terminal window where you tried previous code snippet: `(demo (saw [261.63 329.63]))`. Sounds better, right? Now, if you wonder why I used those exact frequencies you can look at this [note-frequency-chart](https://i.pinimg.com/originals/41/ee/a3/41eea38658a8db1e57e7a444dad10037.png){:target="_blank"} and you will see that I used C4 and E4 notes, which are in harmony. Don't worry if it doesn't make sense now, in future blog posts I will write about basic concepts of music theory as we explore further what Overtone has to offer.

## What's next?
The basic setup described in this article is enough to get everything started, but it's not very practical to type lines of code in terminal and execute them one by one, especially for live coding. In the next article I will cover setting up an editor with Clojure development environment so we can truly use the power that Clojure has to offer.
