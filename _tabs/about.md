---
title: About
icon: fas fa-info
order: 4
---

![Profile Picture](/savo.jpg){:width="400"}

I am Savo Djuric, from Belgrade, a beautiful city on the confluence of Danube and Sava rivers. Belgrade
is  the capital of Serbia, an ex-Yugoslav republic.

I am writing programs in **Clojure** programming language.
I am also a musician, have been playing guitar for about 15 years now, most of it professionaly.

Several of my other interests include running mid-long/long distances, (Cuban) Salsa dancing,
craft beer, European comic books (French-Belgian & Italian scene), travel, history, etc.

This blog contains articles about various things that I'm interested in, but mostly about programming and music.
I will appreciate your comments, suggestions and advice, as it will help me learn & develop my skills.

*All the best!*
